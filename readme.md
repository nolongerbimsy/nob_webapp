# Nob Webapp 

## Background
This Webapp was made to visualize and elaborate on the results of participants of a 
[quantified self study](https://bitbucket.org/nolongerbimsy/), the study was about sleep inertia and 
these factors measured to find out if they would affect a participants sleep.


## Requirements:

#### Software:
- [Python 3](https://www.python.org/) 
- [Bokeh (1.4.0)](https://bokeh.org/)
- [Flask (1.1.1)](https://flask.palletsprojects.com/en/1.1.x/)
- [Pandas (0.25.3)](https://pandas.pydata.org/)
- [matplotlib (3.1.2)](https://matplotlib.org/)
- [scipy (1.4.1)](https://www.scipy.org/)


To install all the required Python 3 packages at once:  

    pip3 install bokeh flask pandas matplotlib scipy

#### Data 

The data comes from the [nob file parser](https://bitbucket.org/nolongerbimsy/nob_file_parser/src/master/). This data is required to be put in an ExampleData folder.      


## Installing 

Clone or download the repository. 

Ensure the data is in the correct folder. (Bokeh/ExampleData/) 

## How to run

Run the webapp.py script for local hosting. Either from the command line or the run function in any python IDE. 


## Presentation, report and DHF
The presentation, report and DHF of the NOB-team can be found in this repository under the filenames NOB presentation.pptx, Report NOB(2).pdf, DHF-NOB.pdf



## Authors 
This webapp was made by the NOB-team. 
 - Sylt Schuurmans 
 - Demy van de Veen 
 - Elibeth Alberts 
 - Yagmur Dogay 
 - Iwan Hidding 

## Disclaimer 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 