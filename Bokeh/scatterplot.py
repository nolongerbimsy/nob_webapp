"""
This script creates the scatterplot based on the input data. It contains a test function with test data and a function
that creates the plot from this class when its called.

author : Iwan Hidding

"""

from bokeh.plotting import figure
from bokeh.models.formatters import DatetimeTickFormatter
from bokeh.models import TapTool, ColumnDataSource, CustomJS
import pandas as pd
import numpy as np
from bokeh.palettes import Spectral4, d3

class ScatterPlotting:
    """
    Setup for the scatterplot that generates the plot showing the cognitive function over the weeks
    for each participant.
    """
    def __init__(self, dataframe):
        self.dataframe = dataframe
        self.xdata = None
        self.ydata = None

    def data_selection(self, colnames="cognitive_function",
                       start_date="2019-11-03", end_date="2019-12-30"):
        """
        Selects the data on column names and the desired date and time,
        this should allow the user to plot any period with any columns.
        """
        self.colnames = colnames

        mask1 = (self.dataframe.index >= start_date) & (self.dataframe.index <= end_date)
        self.xdata = self.dataframe.loc[mask1].index
        self.ydata = self.dataframe[colnames]

    def settings(self, title="Scatterplot of full range"):  # other settings to be added
            """
            This function contains all the options available in the p.scatter function of Bokeh,
            and allows the user the input a title and potential other settings to be added.
            """

            tools_used = "hover,pan,wheel_zoom,zoom_in,zoom_out,box_zoom,undo,redo," \
                         "reset,tap,save,box_select,poly_select,lasso_select,"

            tap = TapTool()

            scatterplot = figure(plot_width=600, plot_height=600, tools=[tap],
                                 x_axis_type='datetime', title=title)

            methods = self.dataframe['Method'].unique()[1:]
            sources = []

            sessions = [1, 2]
            method_average = {}
            x_average = {}
            for data, color, i in zip(methods, d3['Category10'][10], range(0, 4)):
                for session in sessions:

                    xdata1 = self.dataframe[self.dataframe['Session'] == session]
                    xdata = xdata1.loc[xdata1['Method'] == data].index

                    ydata1 = self.dataframe[self.dataframe['Session'] == session]
                    ydata = ydata1[self.colnames][ydata1['Method'] == data].tolist()
                    x_average[data] = xdata1.loc[xdata1['Method'] == data].index[int(len(xdata)/2)]
                    method_average[data] = ydata1[self.colnames][ydata1['Method'] == data].mean()

                    sources.append(ColumnDataSource(data=dict(x=xdata, y=ydata)))

                    scatterplot.circle('x', 'y', source=sources[i*2+(session-1)],
                                       size=10, fill_color=color, fill_alpha=1,
                                       line_color='Black', muted_color=color, line_width=(session - 1) * 1.8,
                                       line_alpha=(0.7),
                                       muted_alpha=0.2, legend_label=data)



            scatterplot.line(x=list(x_average.values()), y=list(method_average.values()), line_width=1.5, line_color='Black')

            # Create a CustomJS callback with the code and the data
            tap.callback = CustomJS(args=dict(sources=sources), code="""
                // Get indices array of all selected items
                for (let i = 0; i < sources.length; i++) { 
                    let selected = sources[i].selected.indices;
                    let data = sources[i].data;
                    if (selected.length > 0) { // Something has been selected within this source
                        // This just prints some information on the selected element (value and source[index])
                        console.log(selected + " selected from data source " + i);
                        console.log("values: [" + data['x'][selected[0]] + ", " + data['y'][selected[0]] + "]");
                        
                        // Process selected element in another JS function by giving the source (index) and
                        // the element position within that source. Use the 'data' variable for passing the actual
                        // numerical value(s).
                        get_new_plot(i, selected);
                    }
                };
                // TODO: selecting across data sources always keeps the last selected element from the other data
                //       source. Solution: global variable (elsewhere) to keep track and only get changed values
            """)

            scatterplot.legend.click_policy = "mute"
            scatterplot.y_range.end = self.ydata.max() * 1.11
            scatterplot.legend.orientation = "horizontal"
            scatterplot.legend.location = "top_center"

            scatterplot.xaxis.major_label_orientation = np.pi / 2
            scatterplot.xaxis.formatter = DatetimeTickFormatter(
                minutes=["%Y-%m-%d %H:%M:%S"],
                hourmin=["%Y-%m-%d %H:%M:%S"],
                hours=["%Y-%m-%d %H:%M:%S"],
                days=["%Y-%m-%d"],
                )

            return scatterplot


def plotting():

    EXAMPLEX = np.array(
        [6, 7, 6, 7, 6, 7, 6, 7, 6, 7, 6, 9, 8, 9, 8, 9, 8, 9, 8, 9, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 11, 12, 11, 12,
         11, 12, 11, 12, 12])
    methods = np.array(['Wake up Lights', 'Wake up Lights', 'Wake up Lights', 'Wake up Lights',
                        'Wake up Lights', 'Wake up Lights', 'Wake up Lights', 'Wake up Lights',
                        'Wake up Lights', 'Wake up Lights', 'Wake up Lights', 'Phone alarm', 'Phone alarm', 'Phone alarm',
                        'Phone alarm', 'Phone alarm', 'Phone alarm', 'Phone alarm', 'Phone alarm',
                        'Phone alarm', 'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app',
                        'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app',
                        'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app',
                        'fitbit smart alarm', 'fitbit smart alarm', 'fitbit smart alarm',
                        'fitbit smart alarm', 'fitbit smart alarm', 'fitbit smart alarm',
                        'fitbit smart alarm', 'fitbit smart alarm', 'fitbit smart alarm'])

    DF_INDEX = pd.date_range(start='9-3-2019', periods=40)
    DF = pd.DataFrame(EXAMPLEX)
    DF = DF.set_index(DF_INDEX)
    DF.columns = ["cognitive_function"]
    DF['Method'] = methods
    SCATTER = ScatterPlotting(DF)
    SCATTER.data_selection()
    plot = SCATTER.settings(title="Scatterplot")
    return plot



def dataplotting(file, title, settings):
    """
    This function takes a file, title and settings and returns the scatterplot matching the input file.
    """
    #print("Plotting using file input")
    df = pd.read_csv(file, index_col=0, parse_dates=[0])
    #print(df.head())
    df['cognitive_function'] = (0.5 * (df['Math_good'] - df['Math_wrong']) / 40 + 0.5 * 200/(df['RT_ave']+df['RT_ave.1'] / 2) * 100)  # df['overall_score']
    #print(df.head())
    scatter = ScatterPlotting(df)
    scatter.data_selection(colnames=settings)
    plot = scatter.settings(title=title)
    return plot

