import pandas as pd
import numpy as np
from bokeh.io import show, output_file
from bokeh.models import ColumnDataSource, FactorRange
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource


class BoxPlotting:
    """
    Setup for the scatterplot that generates the plot showing the cognitive function over the weeks
    for each participant.
    """
    def __init__(self, dataframe1, dataframe2):
        self.listofdataframes = [dataframe1, dataframe2]
        self.xdata = None
        #self.ydata = None
        #self.colnames = None

    def data_selection(self, colnames=["Group_1", "Group_2"],
                       start_date="2019-11-03", end_date="2019-12-30",
                       ):
        """
        Selects the data on column names and the desired date and time,
        this should allow the user to plot any period with any columns.
        """
        #self.colnames = colnames

        #mask1 = (self.dataframe.index >= start_date) & (self.dataframe.index <= end_date)
        #self.xdata = self.dataframe.loc[mask1]
        #self.ydata = self.dataframe[colnames]


    def settings(self, title="Scatterplot of full range"):  # other settings to be added
        """
        This function contains all the options available in the p.scatter function of Bokeh,
        and allows the user the input a title and potential other settings to be added.
        """
        palette = ['#440154', '#30678D', '#35B778', '#FDE724']
        groups = ["Group 1", "Group 2"]
        methods = self.listofdataframes[0]['Method'].unique()
        x = [(method, group) for method in methods for group in groups]

        precounts = []
        for method in methods:
            for dataframe in self.listofdataframes:
                #print(self.dataframe[group][self.dataframe['Method'] == method].size)
                mylist = dataframe['cognitive_function'][dataframe['Method'] == method].mean()

                #myint = dataframe[dataframe['Method'] == method].size
                #adapted_list = [x / myint for x in mylist]
                precounts.append(mylist)
        #print(precounts)
        counts = tuple(precounts)
        print(counts)
        source = ColumnDataSource(data=dict(x=x, counts=counts))

        p = figure(x_range=FactorRange(*x), plot_height=350, title="Boxplot of the methods with cognitive function",
                   toolbar_location=None, tools="")


        p.vbar(x='x', top='counts', width=0.9, source=source, line_color="white",
               fill_color=factor_cmap('x', palette=palette, factors=groups, start=1, end=2))

        MyMean = 0

        for dataframe in self.listofdataframes:
            MyMean += dataframe['cognitive_function'].mean()
        MyMean /= 2

        p.line(x=[-0.5, 12.5], y=MyMean, color="Red", line_width=2, legend="Total average")

        # p.step(x=[-0.5, 1, 'Phone alarm', 'Sleep Cycle app', 11, 12.5], y=[(precounts[0] + precounts[1]) / 2,
        #                                                                    (precounts[0] + precounts[1]) / 2,
        #                                                                    (precounts[2] + precounts[3]) / 2,
        #                                                                    (precounts[4] + precounts[5]) / 2,
        #                                                                    (precounts[6] + precounts[7]) / 2,
        #                                                                    (precounts[6] + precounts[7]) / 2],
        #        line_width=2, mode='center', color="Red", muted_color='Grey', muted_alpha=0.2,
        #        legend="Theoretical real effect")
        p.legend.location = "bottom_left"
        p.legend.click_policy = "mute"

        p.y_range.start = 0
        p.x_range.range_padding = 0.1
        p.xaxis.major_label_orientation = 1
        p.xgrid.grid_line_color = None

        return p


def dataplotting(*files):
    """
    This function calls the class in this script to take the input files and creates a barplot from the data in the file.
    """
    #for file in files:
    df_0 = pd.read_csv(files[0])
    df_1 = pd.read_csv(files[1])
    df_2 = pd.read_csv(files[2])
    df_3 = pd.read_csv(files[3])
    weighted_cog = 0.5
    weighted_RT = 1 - weighted_cog
    df_0['cognitive_function'] = weighted_cog * (df_0['Math_good'] - df_0['Math_wrong']) / 40 + weighted_RT * 200/(df_0['RT_ave']+df_0['RT_ave.1'] / 2) * 100
    df_1['cognitive_function'] = weighted_cog * (df_1['Math_good'] - df_1['Math_wrong']) / 40 + weighted_RT * 200/(df_1['RT_ave']+df_1['RT_ave.1'] / 2) * 100
    df_2['cognitive_function'] = weighted_cog * (df_2['Math_good'] - df_2['Math_wrong']) / 40 + weighted_RT * 200/(df_2['RT_ave']+df_2['RT_ave.1'] / 2) * 100
    df_3['cognitive_function'] = weighted_cog * (df_3['Math_good'] - df_3['Math_wrong']) / 40 + weighted_RT * 200/(df_3['RT_ave']+df_3['RT_ave.1'] / 2) * 100
    df_Group1 = pd.concat([df_0, df_1])
    df_Group2 = pd.concat([df_2, df_3])




    # methods = df_0['Method'].unique()
    # dfbox_0 = pd.DataFrame(columns=methods)
    # dfbox_1 = pd.DataFrame(columns=methods)
    # dfbox_2 = pd.DataFrame(columns=methods)
    # dfbox_3 = pd.DataFrame(columns=methods)
    # dfbox_group1 = pd.DataFrame(columns=methods)
    # dfbox_group2 = pd.DataFrame(columns=methods)
    # for method in methods:
    #     dfbox_0[method] = df_0['cognitive_function'][df_0['Method'] == method] / df_0['cognitive_function'][df_0['Method'] == method].size
    #     dfbox_1[method] = sum(df_1['cognitive_function'][df_1['Method'] == method] / df_1['cognitive_function'][df_1['Method'] == method].size)
    #     dfbox_2[method] = sum(df_2['cognitive_function'][df_2['Method'] == method] / df_2['cognitive_function'][df_2['Method'] == method].size)
    #     dfbox_3[method] = sum(df_3['cognitive_function'][df_3['Method'] == method] / df_3['cognitive_function'][df_3['Method'] == method].size)
    # print(dfbox_0)
    #dfbox_group1 = dfbox_0 + dfbox_1
    #dfbox_group2 = dfbox_2 + dfbox_3
    #print(dfbox_group1)
    #print(dfbox_group2)
    #df_0['Group_1'] = df_0['cognitive_function'] + df_1['cognitive_function']
    #df_0['Group_2'] = df_2['cognitive_function'] + df_3['cognitive_function']

    #print(df_0['Group_1'], df_0['Group_2'])
    #dfboxplot['cognitive_function1'] = calculated_series_1
    #dfboxplot['cognitive_function2'] = calculated_series_2
    #print(df_0['Method'])
    box = BoxPlotting(df_Group1, df_Group2)
    box.data_selection()
    plot = box.settings(title="boxplot")
    return plot

show(dataplotting('ExampleData/Cognitivefunctionstored.csv', 'ExampleData/CognitivefunctionstoredEli.csv',
             'ExampleData/CognitivefunctionstoredDemy.csv', 'ExampleData/CognitivefunctionstoredYagmur.csv'))

