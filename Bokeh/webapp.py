#!/usr/bin/env python3

"""
Full webapp backend code that calls all related functions and returns them to the webapp.
"""

import json
from bokeh.embed import json_item, components
from flask import Flask, render_template, request
from Bokeh import scatterplot
from Bokeh import sleepplot
from Bokeh import boxplot
import pandas as pd
import glob
from Bokeh import Raspheartplot
from bokeh.plotting import gridplot


app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = '/tmp'

feature_names = ["Mood", "Sleepiness", "Cognitive Function"]
feature_participant = ["Profile_1", "Profile_3", "Profile_4", "Profile_7"]
current_feature_participant = "Profile_1"

# Create the main plot
def create_figure(current_feature_name, title, current_feature_participant):
    """
    Create a scatter plot of the selected participant and feature
    :param current_feature_name: the selected feature
    :param current_participant_name: the selected participant
    :return: scatterplot

    """

    print(current_feature_participant)
    file = "ExampleData/{}/daily_data/daily_data_{}.csv".format(current_feature_participant,
                                                                          current_feature_participant[-1])
    plot = scatterplot.dataplotting(file, title, current_feature_name)

    return plot


# Index page
@app.route('/')
def index():
    """
    This function creates the bar graphs that appear by default on the front page.
    """
    """ Renders the index web form with two boxplots
    :return: the rendered view
    """
    plot1 = boxplot.dataplotting('ExampleData/Profile_1/daily_data/daily_data_1.csv',
                                'ExampleData/Profile_3/daily_data/daily_data_3.csv',
                                'ExampleData/Profile_4/daily_data/daily_data_4.csv',
                                'ExampleData/Profile_7/daily_data/daily_data_7.csv',
                                session=1.0)
    plot2 = boxplot.dataplotting('ExampleData/Profile_1/daily_data/daily_data_1.csv',
                                'ExampleData/Profile_3/daily_data/daily_data_3.csv',
                                'ExampleData/Profile_4/daily_data/daily_data_4.csv',
                                'ExampleData/Profile_7/daily_data/daily_data_7.csv',
                                session=2.0)
    grid = gridplot([plot1, plot2], ncols=1)
    # Embed plot into HTML via Flask Render
    script, div = components(grid)
    return render_template('index.html', title='NOB Sleep research', script=script, div=div)

@app.route('/article')
def article():
    """ Renders the article web form
    :return: the rendered view
    """
    return render_template('article.html', title='NOB Sleep research')

@app.route('/research')
def research():
    """ Renders the research web form and creates a scatterplot of the selected participant and feature
    :return: the rendered view
    """

    current_feature_name = request.args.get("feature_name")
    if current_feature_name == None:
        current_feature_name = "Cognitive Function"

    global current_feature_participant
    current_feature_participant = request.args.get("feature_participant")
    if current_feature_participant == None:
        current_feature_participant = "Profile_1"

    new_names = {"Mood": "Mood", "Sleepiness": "KSS", "Cognitive Function": "cognitive_function"}

    plot = create_figure(current_feature_name=new_names[current_feature_name],
                         title="{}'s {}".format(current_feature_participant, current_feature_name).replace("_", " "),
                         current_feature_participant=current_feature_participant)

    script, div = components(plot)
    return render_template("research.html", script=script, div=div,
                           feature_names=feature_names, current_feature_name=current_feature_name,
                           feature_participant=feature_participant,
                           current_feature_participant=current_feature_participant)



@app.route('/contact')
def contact():
    """ Renders the contact web form
    :return: the rendered view
    """
    return render_template('contact.html', title='NOB Sleep research')


@app.route('/get_details', methods=['GET'])
def details_plot():
    """
    This function figures out which data corresponds with the point on the scatterplot that was clicked.
    It returns a grid of the sleep and the raspberry pi plots to appear next to the scatterplot.
    """

    #current_feature_participant = request.args.get("feature_participant")
    print(current_feature_participant)
    #if current_feature_participant == None:
    #    current_feature_participant = "Profile_1"

    source = request.args.get('source')
    index = request.args.get('index')

    df = pd.read_csv("ExampleData/{}/daily_data/daily_data_{}.csv".format(current_feature_participant,
                                                                          current_feature_participant[-1]), index_col=0, parse_dates=True)
    df_1 = df[df['Session'] == 1.0]
    df_2 = df[df['Session'] == 2.0]

    listofmethods = df_1['Method'].unique()

    method = listofmethods[int((int(source)) / 2)]

    if int(source) % 2 == 0:

        date = str(df_1.index[df_1['Method'] == method][int(str(index).split(",")[0])]).split(" ")[0]
    else:
        date = str(df_2.index[df_2['Method'] == method][int(str(index).split(",")[0])]).split(" ")[0]

    pathname = "ExampleData/{}/secondly_data/{}_second_{}*.csv".format(current_feature_participant,
                                                                       current_feature_participant[-1],
                                                                       date)
    print(pathname)
    file = glob.glob(pathname)
    print(file)
    plot = sleepplot.plotting(*file)
    plot2 = Raspheartplot.plotting(*file)
    grid = gridplot([plot, plot2], ncols=1)

    return json.dumps(json_item(grid))

@app.route('/sleep-plot', methods=['POST'])
def sleep_plot():
    """ Dumps the sleep plot
    :return: a json dump of the sleep plot
    """
    plot = sleepplot.plotting()

    return json.dumps(json_item(plot))




if __name__ == '__main__':
    app.run(debug=True)
