from bokeh.plotting import figure, output_file, show
from bokeh.models.formatters import DatetimeTickFormatter
import pandas as pd



class SleepPlotting:
    """
    Setup for the sleepplot, it generates a plot for one night showing the sleep phases.
    """

    def __init__(self, dataframe):
        self.dataframe = dataframe
        self.xdata = None
        self.ydata = None

    def data_selection(self):
        """
              Selects the datetime and the sleep stage to be able to plot the sleep phases per night
              """
        self.xdata = pd.to_datetime(self.dataframe["Unnamed: 0"])
        self.ydata = self.dataframe.level

    def settings(self, colormap, title="Sleep data plot"):  # other settings to be added
        """
        This function contains the settings of the the sleep data plot,
        it allows the user to input a title and colormap and creates the plot.
        :param colormap: The colormap of the sleep plot.
        :param title: title of the sleep plot
        :return: the sleep plot.
        """

        p = figure(plot_width=600, plot_height=300,
                   x_axis_type='datetime', title=title)

        colors = [colormap[x] for x in self.ydata]

        p.line(self.xdata, self.ydata, line_width=0.5, color='grey')

        p.scatter(self.xdata, self.ydata, color=colors)
        p.yaxis.ticker = [0, 1, 2, 3]
        p.yaxis.major_label_overrides = {0: 'deep sleep', 1: 'rem sleep', 2: 'light sleep', 3: 'awake'}


        p.xaxis.formatter = DatetimeTickFormatter(
            minutes=["%H:%M:%S"],
            hourmin=["%H:%M:%S"],
            hours=["%H:%M:%S"],
            days=["%H:%M:%S"],
        )

        return p


def plotting(file="ExampleData/I_2019-11-03_1.csv"):
    """
    This function reads the sleep data and returns the sleep plot
    :param file: path and filename of the sleep data
    :return: the sleep plot.
    """
    df = pd.read_csv(file)
    df = df[::60]
    colormap = {3.0: 'pink',
                2.0: 'lightblue',
                1.0: 'blue',
                0.0: 'darkblue'}


    SCATTER = SleepPlotting(df)
    SCATTER.data_selection()
    plot = SCATTER.settings(colormap, title="Sleep data plot")
    return plot
