from bokeh.models import LinearAxis, Range1d
from bokeh.plotting import figure
import pandas as pd



class LinePlotting:
    """
    Setup for the line plot that generates the plot showing the raspberry and heart rate data over  per day
    for each participant.
    """

    def __init__(self, dataframe, plot_title):
        self.relevant_data = dataframe[["Unnamed: 0", "Air pressure", "Humidity", "Temperature", "Lux", "bpm"]]
        self.relevant_data = self.relevant_data.set_index(
            pd.to_datetime(self.relevant_data[self.relevant_data.columns[0]]))
        self.relevant_data.index.name = "DateTime"
        self.relevant_data = self.relevant_data.drop("Unnamed: 0", axis=1)
        self.relevant_data = self.relevant_data.resample("60S").first()

    def settings(self, plot_title="Background Data"):
        p = figure(plot_width=600, plot_height=300,
                   x_axis_type='datetime', title=plot_title)

        p.y_range = Range1d(0, 150)
        p.extra_y_ranges = {"Air pressure": Range1d(start=(self.relevant_data["Air pressure"].min() - 3),
                                                    end=self.relevant_data["Air pressure"].max() + 20)}
        p.add_layout(LinearAxis(y_range_name="Air pressure"), 'right')
        p.line(self.relevant_data.index, self.relevant_data["Air pressure"], color='#009E73',
               legend_label="Air pressure (hPa)",
               y_range_name="Air pressure", line_width=0.7)
        p.line(self.relevant_data.index, self.relevant_data["Humidity"], color='#CC79A7', legend_label="Humidity (%)",
               line_width=1)
        p.line(self.relevant_data.index, self.relevant_data["Temperature"], color='#D55E00',
               legend_label="Temperature (ºC)", line_width=1)
        p.line(self.relevant_data.index, self.relevant_data["Lux"], color='#000000', legend_label="Light (lx)",
               line_width=1)
        p.line(self.relevant_data.index, self.relevant_data["bpm"], color='#0072B2', line_width=0.7,
               legend_label="Heart rate (bpm)")

        p.legend.location = "top_left"
        p.legend.click_policy = "hide"
        p.xaxis.axis_label = 'DateTime'
        p.yaxis[1].major_label_text_color = '#009E73'
        p.yaxis[1].axis_line_color = '#009E73'
        p.yaxis[0].axis_label = 'Light, Temperature, Humidity and Heart Rate'
        p.yaxis[1].axis_label = 'Air Pressure'
        p.legend.label_text_font_size = "5pt"
        p.legend.orientation = "horizontal"
        return p


def plotting(my_file="ExampleData/I_2019-11-02_0.csv"):
    plot_title = "_".join(my_file.split("/")[-1].split("_")[2:3])
    secondly_data_day_zero = pd.read_csv(my_file)[::60]
    plots = LinePlotting(secondly_data_day_zero, plot_title)
    return plots.settings(plot_title)

