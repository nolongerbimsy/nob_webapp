"""
Script that creates the barplot for the main page of the webapp. Including fake test data.
Author = Iwan Hidding
"""

import pandas as pd
import numpy as np
from bokeh.models import FactorRange
from bokeh.plotting import figure
from bokeh.transform import factor_cmap
from bokeh.models import ColumnDataSource, Range1d, LinearAxis



def testdata():
    """
    A testing function that generates fake data and returns this as a dataframe for the barplot.
    """
    N = 40
    EXAMPLEX1 = np.array(
        [6, 7, 6, 7, 6, 7, 6, 7, 6, 7, 6, 9, 8, 9, 8, 9, 8, 9, 8, 9, 3, 4, 3, 4, 3, 4, 3, 4, 3, 4, 3, 11, 12, 11, 12,
        11, 12, 11, 12, 12])
    EXAMPLEX2 = EXAMPLEX1 +2
    methods = np.array(['Wake up Lights', 'Wake up Lights', 'Wake up Lights', 'Wake up Lights',
                        'Wake up Lights', 'Wake up Lights', 'Wake up Lights', 'Wake up Lights',
                        'Wake up Lights', 'Wake up Lights','Wake up Lights', 'Phone alarm', 'Phone alarm', 'Phone alarm',
                        'Phone alarm', 'Phone alarm', 'Phone alarm', 'Phone alarm', 'Phone alarm',
                        'Phone alarm', 'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app',
                        'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app',
                        'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app', 'Sleep cycle app',
                        'fitbit smart alarm', 'fitbit smart alarm', 'fitbit smart alarm',
                        'fitbit smart alarm', 'fitbit smart alarm', 'fitbit smart alarm',
                        'fitbit smart alarm', 'fitbit smart alarm', 'fitbit smart alarm'])

    DF_INDEX = pd.date_range(start='9-3-2019', periods=40)
    DF = pd.DataFrame(EXAMPLEX1)
    DF = DF.set_index(DF_INDEX)
    DF.columns = ["cognitive_function1"]
    DF['cognitive_function2'] = EXAMPLEX2
    DF['methods'] = methods
    methods = ['Wake up Lights', 'Phone alarm', 'Sleep cycle app', 'fitbit smart alarm']
    return DF



class BoxPlotting:
    """
    Setup for the barplot that generates the plot showing the average cognitive function, mood and kss per method
    for each participant.
    """
    def __init__(self, dataframe):
        self.df = dataframe
        self.xdata = None


    def data_selection(self, colnames=["Group_1", "Group_2"],
                       start_date="2019-11-03", end_date="2019-12-30",
                       ):
        """
        Selects the data on column names and the desired date and time,
        this should allow the user to plot any period with any columns.
        """


    def settings(self, title="Scatterplot of full range"):
        """
        This function contains all the options available in the p.scatter function of Bokeh,
        and allows the user the input a title and potential other settings to be added.
        """
        palette = ['#440154', '#30678D', '#35B778', '#FDE724']

        results1 = ["cognitive_function"]
        results2 = ["Mood", "KSS"]

        methods = self.df['Method'].unique()

        resultsnames = ["Cognitive function", "Mood", "Sleepiness"]
        x = [(result, method) for result in resultsnames for method in methods]

        resultsnames2 = ["Mood", "Sleepiness"]
        x2 = [(result, method) for result in resultsnames2 for method in methods]
        # First range
        precounts1 = []

        for result in results1:
            for method in methods:
                    mylist = self.df[result][self.df['Method'] == method].mean()
                    precounts1.append(mylist)

        counts1 = tuple(precounts1)

        source1 = ColumnDataSource(data=dict(x=x, counts=counts1))

        # Second range
        precounts2 = []

        for result in results2:
            for method in methods:
                mylist = self.df[result][self.df['Method'] == method].mean()
                precounts2.append(mylist)

        counts2 = tuple(precounts2)

        source2 = ColumnDataSource(data=dict(x=x2, counts=counts2))



        p = figure(x_range=FactorRange(*x), plot_height=400,plot_width=700, title=title,  toolbar_location=None, tools="")

        p.extra_y_ranges = {"foo": Range1d(start=0, end=10)}
        p.add_layout(LinearAxis(y_range_name="foo", axis_label="Mood and KSS score"), 'right')


        p.vbar(x='x', top='counts', width=0.9, source=source1, line_color="white",
               fill_color=factor_cmap('x', palette=palette, factors=methods, start=1, end=2))

        p.vbar(x='x', top='counts', width=0.9, source=source2, line_color="white",
               fill_color=factor_cmap('x', palette=palette, factors=methods, start=1, end=2),
               y_range_name="foo"
               )


        p.y_range.start = 0
        p.x_range.range_padding = 0.1
        p.xaxis.major_label_orientation = 1
        p.xgrid.grid_line_color = None

        return p


def dataplotting(*files, session=1):

    df_0 = pd.read_csv(files[0])
    df_1 = pd.read_csv(files[1])
    df_2 = pd.read_csv(files[2])
    df_3 = pd.read_csv(files[3])

    df_0 = df_0[df_0['Session'] == session]
    df_1 = df_1[df_1['Session'] == session]
    df_2 = df_2[df_2['Session'] == session]
    df_3 = df_3[df_3['Session'] == session]
    weighted_cog = 0.5
    weighted_RT = 1 - weighted_cog
    df_0['cognitive_function'] = weighted_cog * (df_0['Math_good'] - df_0['Math_wrong']) / 40 + weighted_RT * 200/(df_0['RT_ave']+df_0['RT_ave.1'] / 2) * 100
    df_1['cognitive_function'] = weighted_cog * (df_1['Math_good'] - df_1['Math_wrong']) / 40 + weighted_RT * 200/(df_1['RT_ave']+df_1['RT_ave.1'] / 2) * 100
    df_2['cognitive_function'] = weighted_cog * (df_2['Math_good'] - df_2['Math_wrong']) / 40 + weighted_RT * 200/(df_2['RT_ave']+df_2['RT_ave.1'] / 2) * 100
    df_3['cognitive_function'] = weighted_cog * (df_3['Math_good'] - df_3['Math_wrong']) / 40 + weighted_RT * 200/(df_3['RT_ave']+df_3['RT_ave.1'] / 2) * 100
    df_Group1 = pd.concat([df_0, df_1, df_2, df_3])

    box = BoxPlotting(df_Group1)
    box.data_selection()
    plot = box.settings(title= "Mean test results {} minutes after alarm time".format(5 if session==1 else 30))


    return plot


