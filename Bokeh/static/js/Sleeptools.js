/**
 * Javascript functions and events
 */

/**
* Fetches some data from some URL given some input
* @param i a number
* @param selected another number
*/
let get_new_plot = function(i, selected) {


    let url = new URL("/get_details", location.href),
        params = {
            source: i,
            index: selected
        };
    // Combine URL and parameters to format a GET-request URL (i.e.: http://localhost:8080/get_details?source=2&index=4)
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    console.log(url);
    fetch(url)
        .then((resp) => resp.json())
        .then(function(plot) {
            // Print what we've received. Update this to receive a new plot object and use 'Bokeh.embed' to include it.
            console.log("Data received:");
            console.log(plot);
            // Empty the div (otherwise plots are appended)
            $("#bokeh-sleep1-plot").html("");
            // Embed the plot in the given div
            Bokeh.embed.embed_item(plot, "bokeh-sleep1-plot");
        })
};




function functionName( s ){
	alert('Hello, ' + s + '!');
}
//
//
//$('#myRadios button').on('click', function() {
//     var selectedValue = $(this).text();
//     console.log(selectedValue);
//});
//
$(function () {

    /* Perform a request for a GC-percentage Bokeh plot for sequences in an uploaded file */
    $("#homepage-submit").click(function (event) {
        // Make sure the form doesn't actually submit
        event.preventDefault();

        // Get the selected file
        let form_data = new FormData();
        form_data.append('week', "1");

        // Get the plot data given the uploaded file
        fetch('/homepage-plot', {
            method: 'POST',
            body: form_data
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (plot) {
                // Empty the div (otherwise plots are appended)
                $("#bokeh-homepage-plot").html("");
                // Embed the plot in the given div
                Bokeh.embed.embed_item(plot, "bokeh-homepage-plot");
            });
    });

    /* Perform a request for a GC-percentage Bokeh plot for sequences in an uploaded file */
    $("#sleep-submit").click(function (event) {
        // Make sure the form doesn't actually submit
        event.preventDefault();

        // Get the selected file
        let form_data = new FormData();
        //form_data.append('week', "1");

        // Get the plot data given the uploaded file
        fetch('/sleep-plot', {
            method: 'POST',
            body: form_data
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (plot) {
                // Empty the div (otherwise plots are appended)
                $("#bokeh-sleep-plot").html("");
                // Embed the plot in the given div
                Bokeh.embed.embed_item(plot, "bokeh-sleep-plot");
            });
    });




});