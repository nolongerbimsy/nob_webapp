"""
This script runs the bootstrap functions for the statistical analysis of our data.
Source : https://github.com/mayer79/Bootstrap-p-values/blob/master/Bootstrap%20p%20values.ipynb
This code was written by Michael Mayer as part of a python bootstrap instructional notebook.
Relevant code has been copied to use for this analysis.
"""

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import pandas as pd


def boot_matrix(z, B):
    """Bootstrap sample

    Returns all bootstrap samples in a matrix"""

    n = len(z)  # sample size
    idz = np.random.randint(0, n, size=(B, n))  # indices to pick for all boostrap samples
    return z[idz]


def bootstrap_mean(x, B=10000, alpha=0.05, plot=False):
    """Bootstrap standard error and (1-alpha)*100% c.i. for the population mean

    Returns bootstrapped standard error and different types of confidence intervals"""

    # Deterministic things
    n = len(x)  # sample size
    orig = x.mean()  # sample mean
    se_mean = x.std() / np.sqrt(n)  # standard error of the mean
    qt = stats.t.ppf(q=1 - alpha / 2, df=n - 1)  # Student quantile

    # Generate boostrap distribution of sample mean
    xboot = boot_matrix(x, B=B)
    sampling_distribution = xboot.mean(axis=1)

    # Standard error and sample quantiles
    se_mean_boot = sampling_distribution.std()
    quantile_boot = np.percentile(sampling_distribution, q=(100 * alpha / 2, 100 * (1 - alpha / 2)))

    # RESULTS
    print("Estimated mean:", orig)
    print("Classic standard error:", se_mean)
    # print("Classic student c.i.:", orig + np.array([-qt, qt]) * se_mean)
    # print("\nBootstrap results:")
    # print("Standard error:", se_mean_boot)
    # print("t-type c.i.:", orig + np.array([-qt, qt]) * se_mean_boot)
    # print("Percentile c.i.:", quantile_boot)
    # print("Basic c.i.:", 2 * orig - quantile_boot[::-1])

    if plot:
        plt.hist(sampling_distribution, bins="fd")


def bootstrap_t_pvalue(x, y, equal_var=False, B=10000, plot=False):
    """Bootstrap p values for two-sample t test

    Returns boostrap p value, test statistics and parametric p value"""

    # Original t test statistic
    orig = stats.ttest_ind(x, y, equal_var=equal_var)

    # Generate boostrap distribution of t statistic
    xboot = boot_matrix(x - x.mean(), B=B)  # important centering step to get sampling distribution under the null
    yboot = boot_matrix(y - y.mean(), B=B)
    sampling_distribution = stats.ttest_ind(xboot, yboot, axis=1, equal_var=equal_var)[0]

    # Calculate proportion of bootstrap samples with at least as strong evidence against null
    p = np.mean(sampling_distribution >= orig[0])

    # RESULTS
    print("p value for null hypothesis of equal population means:")
    print("Parametric:", orig[1])
    print("Bootstrap:", 2 * min(p, 1 - p))

    # Plot bootstrap distribution
    if plot:
        plt.figure()
        plt.hist(sampling_distribution, bins="fd")

np.random.seed(984564) # for reproducability
x = np.random.normal(loc=11, scale=20, size=30)
y = np.random.normal(loc=15, scale=20, size=20)

profilelist = ["Profile_1", "Profile_3", "Profile_4", "Profile_7"]
method1 = ["Phone alarm", "Sleep Cycle app", "Wake up lights"]
method2 = ["Sleep Cycle app", "Wake up lights", "Phone alarm"]
sessions = [1,2]
test = "Mood"

def ttest(method1, method2, sessions, df, test):
    for m1, m2 in zip(method1, method2):
        for s in sessions:
            dfs = df[df["Session"] == s]

            df_method1 = dfs[[test]][dfs["Method"] == m1].dropna()
            df_method2 = dfs[[test]][dfs["Method"] == m2].dropna()

            x = np.array(df_method1)
            y = np.array(df_method2)

            print("pvalue of {} and {} for session {}".format(m1,m2,s))
            bootstrap_t_pvalue(x, y)

            print("mean and std of method {} of session {}".format(m1, s))
            bootstrap_mean(x, B=10000, alpha=0.05, plot=False)
            print("mean and std of method {} of session {}".format(m2, s))
            bootstrap_mean(y, B=10000, alpha=0.05, plot=False)


def ttestsession(df, test):
    df_s1 = df[test][df["Session"] == 1].dropna()
    df_s2 = df[test][df["Session"] == 2].dropna()
    x = np.array(df_s1)
    y = np.array(df_s2)


    print("pvalue of sessions")
    bootstrap_t_pvalue(x, y)

    print("mean and std of session 1")
    bootstrap_mean(x, B=10000, alpha=0.05, plot=False)
    print("mean and std of session 2")
    bootstrap_mean(y, B=10000, alpha=0.05, plot=False)

dfp = pd.DataFrame()
for p in profilelist:
    print("\n" + p +":")
    df = pd.read_csv("Bokeh/ExampleData/{}/{}/daily_data/daily_data_{}.csv".format(p, p, p[-1]))
    df['sleep duration'] = df["deep"] + df["wake"] + df["light"] + df["rem"]
    df = df[['Method', 'Session', test, 'Humidity', 'Temperature', "resting_heart_rate", "sleep duration"]]
    for i in ['Humidity', 'Temperature', "resting_heart_rate", "sleep duration"]:
        quantiles = np.percentile(df[i].dropna(), [25,75])
        iqr = quantiles[1]-quantiles[0]

        lower = quantiles[0]-1.5*iqr
        upper = quantiles[1] + 1.5 * iqr
        mean = (lower + upper)/2
        df[i] = df[i].fillna(mean)
        df = df[df[i]>lower]
        df = df[df[i] < upper]
    df = df[['Method', 'Session', test]]
    ttest(method1, method2, sessions, df, test)
    dfp = dfp.append(df)
print("\naverage")
ttest(method1, method2, sessions, dfp, test)